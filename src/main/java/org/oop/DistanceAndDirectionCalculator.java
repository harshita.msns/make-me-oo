package org.oop;

public class  DistanceAndDirectionCalculator {
    public static double distance(Point from, Point to) {
        return from.distanceBetweenCurrentPointAndToPoint(to);
    }

    public static double direction(Point from, Point to) {
        return from.directionBetweenCurrentPointAndToPoint(to);
    }
}
